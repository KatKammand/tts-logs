@echo off
echo Setting up virtual environment...
python -m venv venv
call .\venv\Scripts\activate

echo Installing requirements...
pip install -r requirements.txt

echo.
echo Press any key to close the terminal.
pause >nul