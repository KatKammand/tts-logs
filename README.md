# Log file monitor that voices lines with Coqui TTS

Designed for SL logs specifically.

[Coqui-ai Github Page](https://github.com/coqui-ai/TTS)

## Installation

### 0. Clone this repository (need GIT for that) or download it as .zip
- Using git to clone it you will have an easier time getting updates.
  - If you want to git clone, download here: [Git download](https://git-scm.com/downloads)
    - Once installed (you can use default options on all for it), create a folder you want to install TTS Server in.
    - Open the folder in terminal, then run command:
```bash
git clone https://gitlab.com/KatKammand/tts-logs.git
```
    - When you want to get any updates to the TTS Server, open the `tts-api-server` folder in terminal run
```bash
git pull
```    
      - This will get the updates from the gitlab repository.

  - If you download the `.zip`, unpack it into the folder you want. You will manually need to get the updates from the gitlab page with same method in future. 


### 1. Check if you have python installed, its version and requirements
In terminal run:
```bash
python --version
```

Version `3.11.x` is needed, if you have newer or older, need to take it into account. Either change to 3.11 version, or run it in virtual python environment with the 3.11 verison.

Install Python (programming language used for many AI modules) version 3.11.6 [Link here](https://www.python.org/downloads/release/python-3116/) 
- Choose `"Use admin privileges when installing py.exe"` AND `"Add python.exe to PATH"`
  - If you dont choose both, i assume you know what you are doing and are able to take care manual PATHing yourself
  - If asked about `"disable path length limit"` or something like that at any point, say yes.
- Then just click "Install Now"

- When its done, open terminal and run command:
```bash
python --version
```
It should say `Python 3.11.6`. If it does, everything was done correctly, move to next step.
If it doesnt find Python, make sure you close all terminals, then open and try again. If it still isnt found, run the installer again and make sure you click `Add python.exe to PATH`
- Restart is one way to makes sure all terminals are closed too.
- If you have wrong version, you might need to uninstall old versions in Control Panel first, then install the 3.11.6 again.

### 2. C++ Required for CPython --> AI model running
When AI is involved, C++ is involved. Python uses C++ under the hood to run the AI models.

You might have it installed already by some other software, so you can skip this step for now, and return to it if you get an error asking for this in step `4.`

[Visual studio C++ build tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/)

### 3. Install `Espeak NG`, Text-to-speech framework

For current selected Coqui tts models need to install
- Go to [Espeak Ng v.1.51](https://github.com/espeak-ng/espeak-ng/releases/tag/1.51)
  - Bottom of page, from Asset parts `espeak-ng-X64.msi` usually correct for windows users.
  - Run the installer, default values on all

- Espeak NG is an open source tts framework, see its Github [here for more info if interested.](https://github.com/espeak-ng/espeak-ng).

### 4. Install the requirements run the following command in project root directory.

For windows users, see the `win-installer.bat` and double click it (installer script for the commands below)

- If during install script you get error `ERROR: Could not build wheels for TTS, which is required to install pyproject.toml-based projects`
  - You need to install [Microsoft C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/)
    - See Step 2. for explanation if interested, or just follow these steps.
    - Download and run the installer
    - Select `Desktop development with C++` & Install
      - Once it is done it will say `All installations are up to date`, with `Visual Studio Build Tools 2022` with Modify/Lanuch/More options --> Close the window. 
  - Run `win-installer.bat` again. 
    - This time it should complete without errors. If it doesnt... well these worked for me.

### Successfull install should finish with something like this in the logs:

```
-3.5.0 tokenizers-0.19.1 torch-2.3.0 torchaudio-2.3.0 tqdm-4.66.4 trainer-0.0.36 transformers-4.41.0 typeguard-4.2.1 typer-0.9.4 typing-extensions-4.11.0 tzdata-2024.1 tzlocal-5.2 umap-learn-0.5.6 unidecode-1.3.8 urllib3-2.2.1 wasabi-1.1.2 watchdog-4.0.0 weasel-0.3.4 yarl-1.9.4

Press any key to close the terminal.
```


If you want to run the commands manually or are Unix/MacOS user, see below:

when using venv on Windows
```bash
python -m venv venv
.\venv\Scripts\activate
pip install -r requirements.txt
```

or, when using venv on Unix/MacOS

```bash
python -m venv venv
source venv/bin/activate 
pip install -r requirements.txt
```

to leave the virtual environment, use the `deactivate` command.

If you run into error with TTS installation (see steps above)
  - You need to install [Microsoft C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/)

Or if you dont want to run it in virutal env for some reason:
```bash
pip install -r requirements.txt
```


## Starting Server
Takes about 1 GB of RAM since it has 1 model set loaded.

On first run of server, the model is downloaded and it may take a minute or two.
The model will be downloaded (from the used model repositories, see the linked ones below for more information on them)

For windows users, double click the `win-start.bat` file to start the server.
- If everything went as it was supposed to, after downloads and startup, you should have the following log line:



For Unix/MacOS users, go to this project root `tts-api-server`, and run the following commands from terminal:
```bash
python -m venv venv
source venv/bin/activate 
python -m tts_api_server
```

POTENTIAL ERROR CASE:
- If you get `ModuleNotFoundError: No module named 'torch._prims_common` or something like that, you need to install `pytorch` manually
  - Might work if you just remove the `venv` folder and everything in it, and run the installer again
  - OR head to the project root directory in terminal
    1. enter the venv with `.\venv\Scripts\activate`
    2. uninstall pytorch with `pip uninstall pytorch` and `y` to confirm it
    3. reinstall pytorch with `pip install pytorch`
    4. now try to run the `win-start.bat`



Your file monitor should now be running and ready to use, keep the terminal open, can leave it minimized. When you want to close/stop the file monitor, just close the terminal.

You probably also want to make a startup shortcut of the `win-start.bat` as windows user, see next section for that.

Properly running server should look like this:
```
---------------------------------------------------
  Activating Virtual Environment...
---------------------------------------------------
<other stuff>
 | > do_amp_to_db_linear:True
 | > do_amp_to_db_mel:True
 | > do_rms_norm:False
 | > db_level:None
 | > stats_path:None
 | > base:10
 | > hop_length:256
 | > win_length:1024
 > initialization of speaker-embedding layers.
```

Now if your "file_monitor_config.json" is properly set, your logs will be read and should hear voices from people you've set them for.

## file monitor Autostartup

For windows users, to make a start shortcut of the `win-start.bat`, right click the file, `Show more options` if Win11, click `Send to --> Desktop (Create Shortcut)`, if you want desktop shortcut, or `Create Shortcut`, then copy paste it to whatever location you want.

If you want to make the file monitor automatically start at startup, see [How To Geek guide](https://www.howtogeek.com/754239/how-to-access-the-windows-10-startup-folder/)
- TL:DR
  - Windows + R --> `shell:startup` --> drop a shortcut of `win-start.bat` to the startup folder.


## Developing the server

Do the install requirements and steps, then run the server in autoreload mode with:

```bash
python -m venv venv
source venv/bin/activate 
python -m tts_api_server -r
```

```
usage: tts_api_server [-h] [-o HOST] [-p PORT]

Run Silero within a FastAPI application

options:
  -h, --help            show this help message and exit
  -o HOST, --host HOST
  -p PORT, --port PORT
  -r, --reload          Enable automatic reloading on file change
```



[Silero GitHub Page](https://github.com/snakers4/silero-models)

[Coqui-ai Github Page](https://github.com/coqui-ai/TTS)

# API Docs
API Docs can be accessed from [http://localhost:8001/docs](http://localhost:8001/docs)

