@echo off
echo Fetching updates from gitlabs, if you have git installed
git pull

echo.
echo Press any key to close the terminal.
pause >nul