from typing import List, Dict

class PersonConfig:
    def __init__(self, char_id: str, voice_id: str, voice_nearby: bool, voice_im: bool) -> None:
        self.char_id = char_id
        self.voice_id = voice_id
        self.voice_nearby = voice_nearby
        self.voice_im = voice_im

class MonitorConfig:
    def _init_(self) -> None:
        self.folder_path = ''
        self.default_voice_id = ''
        self.read_online_notifications = False,
        self.people_configs: List[PersonConfig] = []
    
    def json_read(self, data: Dict):
        self.folder_path = data.get('folder_path')
        self.default_voice_id = data.get('default_voice_id')
        self.read_online_notifications = data.get('read_online_notifications', False)
        self.people_configs: List[PersonConfig] = []

        people_configs = data.get('people_configs', [])
        for p in people_configs:
            new_person = PersonConfig(
                    char_id=p.get('id'), 
                    voice_id=p.get('voice_id', self.default_voice_id),
                    voice_nearby=p.get('voice_nearby', True),
                    voice_im=p.get('voice_im', False)
                )
            self.people_configs.append(new_person)

