#[YYYY/MM/DD HH:MM:SS]  Name (name_id): message
#[YYYY/MM/DD HH:MM:SS]  Name (name_id): /me message


import re
from datetime import datetime

nearby_chat_log = "chat-"
sl_system = "Second Life"
emote_prefix = "/me"

def clean_emote(input_string: str):
    cleaned_list = input_string.split(emote_prefix, 1)
    return "".join(cleaned_list)

def extract_parts(input_string: str):
    # Remove the first 21 characters from the string
    trimmed_string = input_string[21:]
    
    pattern = r"^(.*?)(?: \((.*?)\))?(?:: (.*))?$"

    match = re.search(pattern, trimmed_string)

    if match == None:
        return None, None, None
        
    name_or_all = match.group(1).strip()  # Trim leading/trailing spaces
    string_between_parentheses = match.group(2) if match.group(2) else None
    message_after_colon = match.group(3) if match.group(3) else None
    
    if(message_after_colon != None and message_after_colon != emote_prefix):
        message_after_colon = clean_emote(message_after_colon)
    
    return name_or_all, string_between_parentheses, message_after_colon



def get_current_date():
    current_date = datetime.now()
    # Format the date as "YYYY-MM-DD"
    return current_date.strftime("%Y-%m-%d")

def get_year_month():
    current_date = datetime.now()
    # Format the date as "YYYY-MM"
    return current_date.strftime("%Y-%m")

def is_online_announcement(message: str | None):
    return (message == "is online." or message == "is offline.")

def is_file_ids(file_name: str, id: str) -> bool:
    # "name thing" and "name.thing" --> name_thing
    dot_corrected_id = id.replace('.', '_').replace(' ', '_').lower()
    return file_name.lower().startswith(dot_corrected_id)

def is_file_nearby(file_name:str) -> bool:
    return file_name.startswith(nearby_chat_log)

def replace_urls(text: str) -> str:
    url_pattern = re.compile(
        r'((?:https?://|www\.)'  # Match starting with http://, https://, or www.
        r'[a-zA-Z0-9.-]+'        # Domain name
        r'(?:/[^\s]*)?)'         # Remaining URL path
    )
    replaced_text = re.sub(url_pattern, 'link', text)

    return replaced_text

def replace_item_shares(text: str) -> str:
    clean_pattern = re.compile(r'^secondlife:///app/.+/completename')
    return re.sub(clean_pattern, '', text)

def replace_profile_links(text: str) -> str:
        clean_pattern = re.compile(r'^secondlife:///app/.+/about')
        return re.sub(clean_pattern, 'profile link', text)

def special_char_filter(text: str) -> str:
    # Define a pattern to match all characters except letters, digits, spaces, and common punctuation
    clean_pattern = re.compile(r'[^a-zA-Z0-9\s.,!?\'"-]')
    
    # Substitute matched characters with an empty string
    cleaned_text = re.sub(clean_pattern, '', text)
    
    return cleaned_text

def text_cleaner(text: str) -> str:
    url_cleaned = replace_urls(text)
    item_shar_clean = replace_item_shares(url_cleaned)
    profile_cleaned = replace_profile_links(item_shar_clean)
    return special_char_filter(profile_cleaned).strip()