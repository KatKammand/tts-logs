from watchdog.observers import Observer
from typing import List, Dict
from watchdog.events import FileSystemEventHandler
from tts_log_reader.sl.sl_parser import is_file_nearby, is_file_ids, extract_parts, is_online_announcement, text_cleaner
from tts_log_reader.sl.sl_classes import PersonConfig, MonitorConfig
import threading
import json
import os
import time

import winsound

# Replace 'your_audio_file.wav' with the path to your .wav file
def play_wav(path):
    winsound.PlaySound(path, winsound.SND_FILENAME)


config_file = '../../file_monitor_config.json'
project_root = os.path.dirname(os.path.abspath(__file__))
config_path = os.path.join(project_root, config_file)

#change to "path.startswith(monitored_id)"
#then need to have a list of last file positions for each recently read file


class FileWatcher(FileSystemEventHandler):
    monitored_files: Dict[str, int] = {}
    monitored_ids: List[PersonConfig] = []
    read_onlines = False

    def __init__(self, monitor_config: MonitorConfig, voice_generator):
        self.voice_generator = voice_generator
        self.folder_path = monitor_config.folder_path
        self.default_voice_id = monitor_config.default_voice_id
        self.monitored_ids = monitor_config.people_configs
        self.read_onlines = monitor_config.read_online_notifications

    def is_monitored_file(self, modified_file_path: str) -> bool:
        file_name = os.path.basename(modified_file_path)
        # check if nearby
        if(is_file_nearby(file_name)):
            return True

        for x in self.monitored_ids:
            if x.voice_im and is_file_ids(file_name=file_name, id=x.char_id):
                return True

        return False

    def on_modified(self, event):
        if(event.event_type != 'modified'):
            return
        
        file_path = event.src_path
        if(self.is_monitored_file(file_path)):
            self.get_next_line(file_path)       


    def get_last_position(self, file) -> int:
        file.seek(0, 2)  # Move the pointer to the end of the file
        position = file.tell()
        new_position = position
        
        # file ends in \n so want second last
        if(new_position < 2):
            return 0

        count = 0
        try:
            while new_position > 0 and count < 3:
                new_position -= 1
                file.seek(new_position)
                if file.read(1) == '\n':
                    count += 1
        except Exception as e:
            print(f"An exception occurred: {type(e).__name__}")
            print(f"While reading file: {file.name} and char: {file.read(1)}")
            return position

        return new_position + 1

    def get_next_line(self, file_path: str):
        file_name = os.path.basename(file_path)
        is_nearby = is_file_nearby(file_name=file_name)
        last_pos = self.monitored_files.get(file_name)

        with open(file_path, 'r', encoding="utf-8") as file:
            if(last_pos == None):
                last_pos = self.get_last_position(file)
            
            file.seek(last_pos)  # Move to the last read position
            for line in file:
                self.read_next_line(line, is_nearby=is_nearby)
                
            new_pos = file.tell()
            self.monitored_files[file_name] = new_pos

    def read_next_line(self, line: str, is_nearby: bool):
        print("Callback: " + line)
        name, id, message = extract_parts(line)
        print(name, id, message)

        if(id == None and name != None):
            id = name

        is_online_msg = is_online_announcement(message=message)
        if(is_online_msg and not self.read_onlines): return

        id_config = None
        for x in self.monitored_ids:
            if x.char_id.lower() == str(id).lower() and ((is_nearby and x.voice_nearby) or (not is_nearby and x.voice_im)):
                id_config = x

        if(not id_config and not is_online_msg):
            return

        voiced_message = message
        if(voiced_message == None):
            voiced_message = name

        # in IM --> name part has the message and onliners name --> taken care of by 86
        # in nearby, name has onliners name, and message has "is online."/"is offline." so want both
        if(is_online_msg and name != None and message != None):
            voiced_message = name + " " + message 


        if(voiced_message == None):
            return
    
        voice_id = self.default_voice_id
        if(id_config and id_config.voice_id):
            voice_id = id_config.voice_id

        cleaned_message = text_cleaner(voiced_message)
        if(len(cleaned_message) < 1):
            return

        self.play_line(voice_id=voice_id, text=cleaned_message)

    
    def play_line(self, text: str, voice_id):
        try:
            audio = self.voice_generator(voice_id=voice_id, text=text)
            play_wav(str(audio))
            audio.unlink()    
        except Exception as e:
            print(f"An exception occurred: {type(e).__name__}")  
            
def monitor_loop(voice_generator):
    with open(config_path, 'r', encoding='utf-8') as file:
            data = json.load(file)

    if(data == None): 
        print('Missing file montior config file')
        return

    config = MonitorConfig()
    config.json_read(data)

    event_handler = FileWatcher(voice_generator=voice_generator, monitor_config=config)
    observer = Observer()
    observer.schedule(event_handler=event_handler, path=config.folder_path, recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(10)
    finally:
        observer.stop()
        observer.join()

def start_file_monitoring(voice_generator, as_thread: bool):
    if(as_thread):
        thread = threading.Thread(target=monitor_loop, args=([voice_generator]), daemon=True)
        thread.start()
    else:
        monitor_loop(voice_generator)
