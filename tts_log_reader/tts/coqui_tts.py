from TTS.api import TTS
import torch 
from pathlib import Path
from tts_log_reader.utils import generate_unique_filename, ensure_directory_exists




class CoquiTtsService:
    """
    Generate TTS wav files using Coqui
    """
    tts_name = 'coqui_tts'
    
    def __init__(self, save_path) -> None:
        self.save_path = save_path
        device = "cuda" if torch.cuda.is_available() else "cpu"
        model_name = "tts_models/en/vctk/vits"
        self.model = TTS(model_name).to(device)
        
    def generate_file(self, voice_id: str, text: str, file_path: str):
        try:
            return Path(self.model.tts_to_file(text=text, speaker=voice_id, file_path=file_path))
        except FileNotFoundError:
            ensure_directory_exists(file_path=file_path)
            return Path(self.model.tts_to_file(text=text, speaker=voice_id, file_path=file_path))
        
    def generate(self, voice_id, text: str):
        file_path = f"{self.save_path}/{generate_unique_filename()}"
        return self.generate_file(text=text, voice_id=voice_id, file_path=file_path)

        #return coqui_tts.tts(text=text, speaker=voice_id)
    
    def generate_samples(self, text: str):
        for voice_id in self.model.speakers:
            if(voice_id == 'ED\n'): continue
            
            file_path = f"{self.save_path}/samples/coqui_{voice_id}.wav"
            self.generate_file(text=text, voice_id=voice_id, file_path=file_path)
