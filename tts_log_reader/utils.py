import os
from pathlib import Path
import uuid
from datetime import datetime

def cleanup_file(path):
    os.remove(path)
    
def generate_unique_filename(file_extension=".wav"):
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    unique_id = str(uuid.uuid4().hex)[:8]  # Use the first 8 characters of the UUID
    filename = f"{timestamp}_{unique_id}{file_extension}"
    return filename

# creates the required folders for the file path
def ensure_directory_exists(file_path: str):
    Path(file_path).parent.mkdir(parents=True, exist_ok=True)
