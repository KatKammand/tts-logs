import argparse
import pathlib
import os
from tts_log_reader.tts.coqui_tts import CoquiTtsService
from tts_log_reader.sl.file_monitor import start_file_monitoring

parser = argparse.ArgumentParser(
                    prog='tts_log_reader',
                    description='Run Coqui for log reading')
parser.add_argument('-s', '--sample', action='store', dest='sample', default=False)
parser.add_argument('-r', '--reload', action='store_true', dest='reload', help='Enable automatic reloading on file change')


args = parser.parse_args()


# Read and use the USE_SERVICE_1 and USE_SERVICE_2 variables

module_path = pathlib.Path(__file__).resolve().parent
os.chdir(module_path)
TEMP_PATH = pathlib.Path("temp")

if not TEMP_PATH.exists():
    TEMP_PATH.mkdir()

save_path = f"{module_path}//{TEMP_PATH}"

coqui_tts = CoquiTtsService(save_path)

if(args.sample):
    coqui_tts.generate_samples("This is a sample text, the file names are the voice id. If you like my voice, use it for someone!")
else:
    start_file_monitoring(voice_generator=coqui_tts.generate, as_thread=False)
