@echo off

echo ---------------------------------------------------
echo   Activating Virtual Environment...
echo ---------------------------------------------------
call .\venv\Scripts\activate

echo ---------------------------------------------------
echo   Creating TTS audio samples...
echo ---------------------------------------------------
python -m tts_log_reader -s True

echo.
echo ---------------------------------------------------
echo   See ./tts_log_reader/temp/samples folder
echo ---------------------------------------------------
echo Press any key to close the terminal.
pause >nul