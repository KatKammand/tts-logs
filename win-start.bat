@echo off

echo ---------------------------------------------------
echo   Activating Virtual Environment...
echo ---------------------------------------------------
call .\venv\Scripts\activate

echo ---------------------------------------------------
echo   Starting TTS Log Reader...
echo ---------------------------------------------------
python -m tts_log_reader

echo.
echo Press any key to close the terminal.
pause >nul